import React from 'react';


function Film(props) {
    const {title, date, img} = props;
    return (
        <div className="col-12 col-md-6 col-lg-4">
            <div className="card">
                <div className="card-body"><h5>{title}</h5>
                    <p className="card-text">Год выпуска: {date}</p>
                    <img src={img} alt=""/>
                </div>
            </div>
        </div>
    )
}

export default Film;