import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';

const Header = () => (

    <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
        <div className="container">
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo01"
                    aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                <p className="navbar-brand mt-3" href="#">CinemaOnline</p>
                <ul className="navbar-nav ml-auto  mt-lg-0">
                    <li className="nav-item active">
                        <a className="nav-link" href="https://cinematica.kg/movies">Фильмы <span
                            className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="https://cinematica.kg/cinema">Кинотеатры</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="https://cinematica.kg/news">Анонсы</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
);


export default Header