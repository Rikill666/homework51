import React from 'react';
import './Film.css';

function Film(props) {
    return (
        <div
            className="film">
            <h1>{props.title}</h1>
            <p>Дата выпуска: {props.date}</p>
            <img src={props.img} alt=""/>
        </div>);
}

export default Film;